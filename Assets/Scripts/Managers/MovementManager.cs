﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementManager : MonoBehaviour
{
    [Header("Controllers")]
    public MovementController movementController = null;   
    public List<RotatorController> rotators = new List<RotatorController>();

    private Vector3 lastMovementDirection = new Vector3();

    // Start is called before the first frame update
    void Start()
    {
        movementController = GetComponent<MovementController>();
        rotators = new List<RotatorController>(GetComponents<RotatorController>());
    }

    // Update is called once per frame
    void Update()
    {
        for(int i = 0; i < rotators.Count; i++)
        {
            if(rotators[i].mode == RotatorController.ENRotationMode.UseDirection)
            {
                if (lastMovementDirection.sqrMagnitude < 0.1f) return;
                rotators[i].lookupDirection = gameObject.transform.position + lastMovementDirection;
            }
        }
    }

    public void setMoveDirection(Vector3 dir)
    {
        lastMovementDirection = dir.normalized;

        if (movementController == null) return;
        movementController.setMovementVector(dir);
    }
    public void jump()
    {
        if (movementController == null || movementController.getMovementType() != MovementController.ENMovementType.Ground) return;
        ((GroundMovementController)movementController).jump();
        
    }
}

