﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public CharacterManager character = null;

    protected Vector3 inputVector;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (character == null) return;

        inputVector.x = Input.GetAxis("Horizontal");
        inputVector.y = 0.0f;
        inputVector.z = Input.GetAxis("Vertical");

        character.movementManager.setMoveDirection(inputVector);
        if (Input.GetKeyDown(KeyCode.Space)) character.movementManager.jump();

    }
}
